import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import VueSocketIO from 'vue-socket.io'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@fortawesome/fontawesome-free/css/all.css'
import bd from '@/firebase/init'
import axios from "axios";
window.event = new Vue({});
Vue.config.productionTip = false
Vue.use(new VueSocketIO({
  debug: true,
  connection: 'http://190.162.176.66:5002/',
}))
new Vue({
  router,
  store,
  vuetify,
  bd,
  axios,
  render: h => h(App)
}).$mount('#app')
