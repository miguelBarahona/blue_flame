import firebase from 'firebase'
import firestore from 'firebase'

const firebaseConfig = {
    apiKey: "AIzaSyA3AjEMBfyZp8klCyq9wYUkvw1tEIZoVz8",
    authDomain: "blue-flame-54628.firebaseapp.com",
    databaseURL: "https://blue-flame-54628.firebaseio.com",
    projectId: "blue-flame-54628",
    storageBucket: "blue-flame-54628.appspot.com",
    messagingSenderId: "767742380317",
    appId: "1:767742380317:web:13e1564ccee506865339f4",
    measurementId: "G-CXND8M6YX9"
  };
  // Initialize Firebase
  const firebaseApp = firebase.initializeApp(firebaseConfig);
  firebase.analytics();
  export default firebaseApp.firestore();