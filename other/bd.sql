-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-01-2020 a las 22:05:26
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de datos: `blue_flame`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adventure`
--

CREATE TABLE `adventure` (
  `id_adventure` int(11) NOT NULL,
  `id_board` int(11) NOT NULL,
  `name_adventure` varchar(30) NOT NULL,
  `imagen_adventure` varchar(100) NOT NULL,
  `password_adventure` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adventure`
--

INSERT INTO `adventure` (`id_adventure`, `id_board`, `name_adventure`, `imagen_adventure`, `password_adventure`) VALUES
(1, 1, 'Lamia Adventure', 'https://www.ufo-spain.com/wp-content/uploads/2019/07/palacio-portada.jpg', '123456');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adventure_user`
--

CREATE TABLE `adventure_user` (
  `adventure` int(11) NOT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `adventure_user`
--

INSERT INTO `adventure_user` (`adventure`, `user`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adventure_user_2`
--

CREATE TABLE `adventure_user_2` (
  `adventure` int(11) NOT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `file_folder`
--

CREATE TABLE `file_folder` (
  `id_file_folder` int(11) NOT NULL,
  `name_file` varchar(100) NOT NULL,
  `url_file` varchar(100) NOT NULL,
  `folder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `folder`
--

CREATE TABLE `folder` (
  `id_folder` int(11) NOT NULL,
  `id_father_folder` int(11) NOT NULL,
  `name_folder` varchar(60) NOT NULL,
  `user` int(11) NOT NULL,
  `folder_father` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `folder`
--

INSERT INTO `folder` (`id_folder`, `id_father_folder`, `name_folder`, `user`, `folder_father`) VALUES
(1, 0, 'new folder', 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id_user`, `name`, `last_name`, `email`, `password`) VALUES
(1, 'miguel', 'barahona', 'miguel.barahona.12@hotmail.com', '123456');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `adventure`
--
ALTER TABLE `adventure`
  ADD PRIMARY KEY (`id_adventure`);

--
-- Indices de la tabla `adventure_user`
--
ALTER TABLE `adventure_user`
  ADD PRIMARY KEY (`adventure`,`user`),
  ADD KEY `idx_adventure_user` (`user`);

--
-- Indices de la tabla `adventure_user_2`
--
ALTER TABLE `adventure_user_2`
  ADD PRIMARY KEY (`adventure`,`user`),
  ADD KEY `idx_adventure_user_2` (`user`);

--
-- Indices de la tabla `file_folder`
--
ALTER TABLE `file_folder`
  ADD PRIMARY KEY (`id_file_folder`),
  ADD KEY `idx_file_folder__folder` (`folder`);

--
-- Indices de la tabla `folder`
--
ALTER TABLE `folder`
  ADD PRIMARY KEY (`id_folder`),
  ADD KEY `idx_folder__folder_father` (`folder_father`),
  ADD KEY `idx_folder__user` (`user`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `adventure`
--
ALTER TABLE `adventure`
  MODIFY `id_adventure` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `file_folder`
--
ALTER TABLE `file_folder`
  MODIFY `id_file_folder` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `folder`
--
ALTER TABLE `folder`
  MODIFY `id_folder` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `adventure_user`
--
ALTER TABLE `adventure_user`
  ADD CONSTRAINT `fk_adventure_user__adventure` FOREIGN KEY (`adventure`) REFERENCES `adventure` (`id_adventure`),
  ADD CONSTRAINT `fk_adventure_user__user` FOREIGN KEY (`user`) REFERENCES `user` (`id_user`);

--
-- Filtros para la tabla `adventure_user_2`
--
ALTER TABLE `adventure_user_2`
  ADD CONSTRAINT `fk_adventure_user_2__adventure` FOREIGN KEY (`adventure`) REFERENCES `adventure` (`id_adventure`),
  ADD CONSTRAINT `fk_adventure_user_2__user` FOREIGN KEY (`user`) REFERENCES `user` (`id_user`);

--
-- Filtros para la tabla `file_folder`
--
ALTER TABLE `file_folder`
  ADD CONSTRAINT `fk_file_folder__folder` FOREIGN KEY (`folder`) REFERENCES `folder` (`id_folder`) ON DELETE CASCADE;

--
-- Filtros para la tabla `folder`
--
ALTER TABLE `folder`
  ADD CONSTRAINT `fk_folder__folder_father` FOREIGN KEY (`folder_father`) REFERENCES `folder` (`id_folder`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_folder__user` FOREIGN KEY (`user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE;
COMMIT;
