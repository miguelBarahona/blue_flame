from pony.orm import PrimaryKey, Database, Required, Set, Optional


db = Database()


class User(db.Entity):
    id_user = PrimaryKey(int, auto=True)
    name = Required(str, 20)
    last_name = Required(str, 20)
    email = Required(str, 50)
    password = Required(str, 100)
    user__adventures = Set('User_Adventure')


class Adventure(db.Entity):
    id_adventure = PrimaryKey(int, auto=True)
    id_board = Required(int)
    name_adventure = Required(str, 30)
    imagen_adventure = Optional(str, 100)
    password_adventure = Required(str, 100)
    user__adventures = Set('User_Adventure')


class User_Adventure(db.Entity):
    user = Required(User)
    adventure = Required(Adventure)

