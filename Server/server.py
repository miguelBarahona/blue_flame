from flask import Flask, jsonify, request
from flask_cors import CORS
from flask_mysqldb import MySQL
from pony.orm import *
from pony.orm.serialization import to_dict
from werkzeug import secure_filename
from random import randint
from flask_socketio import SocketIO, emit, join_room, leave_room
from os import environ
import json
import os

app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
socketio = SocketIO(app, cors_allowed_origins="*")
db = Database()
db.bind(provider='mysql', user='root', password='', host='localhost', database='blue_flame')

UPLOAD_FOLDER = '../public'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['PORT'] = 80
DOMAIN = environ.get('DOMAIN')


class User(db.Entity):
    id_user = PrimaryKey(int, auto=True)
    name = Required(str, 20)
    last_name = Required(str, 20)
    email = Required(str, 50)
    password = Required(str, 100)
    folders = Set('Folder')
    adventures_masters = Set('Adventure', reverse='masters')
    adventures_players = Set('Adventure', reverse='players')


class Adventure(db.Entity):
    id_adventure = PrimaryKey(int, auto=True)
    name_adventure = Required(str, 30)
    imagen_adventure = Optional(str, 100)
    password_adventure = Required(str, 100)
    masters = Set(User, reverse='adventures_masters')
    players = Set(User, reverse='adventures_players')
    boards = Set('Board')


class Folder(db.Entity):
    id_folder = PrimaryKey(int, auto=True)
    id_father_folder = Required(int, default=0)
    name_folder = Required(str, 60)
    user = Required(User)
    file_folders = Set('File_folder')
    folder_father = Optional('Folder', reverse='folders_son')
    folders_son = Set('Folder', reverse='folder_father')


class File_folder(db.Entity):
    id_file_folder = PrimaryKey(int, auto=True)
    name_file = Required(str, 100)
    url_file = Required(str, 100)
    folder = Required(Folder)


class Board(db.Entity):
    id_board = PrimaryKey(int, auto=True)
    adventure = Optional(Adventure)
    orden = Required(int)
    tokens = Set('Token')


class Token(db.Entity):
    id_token = PrimaryKey(int, auto=True)
    board = Optional(Board)
    x = Optional(int)
    y = Optional(int)
    width = Optional(int)
    height = Optional(int)
    src = Optional(str)
    type = Optional(int)

set_sql_debug(False)
db.generate_mapping() 

@app.route('/api/v1.0/insertUsers', methods=['POST'])
@db_session 
def insertUsers():
    if request.method == 'POST':
        json_data = json.loads(request.data)
        email  = json_data['mail']
        password    = json_data['password']
        first_name  = json_data['first_name']
        last_name    = json_data['last_name']
        with db_session:
            new_adventure = User(name=first_name, last_name=last_name, email=email, password=password)
            return '200'

@app.route('/api/v1.0/getIdUser', methods=['POST'])
@db_session
def getIdUser():
    if request.method == 'POST':
        json_data = json.loads(request.data)
        with db_session:
            email = json_data['mail']
            print(email)
            user = User.get(email=email)
            return (user.to_dict(related_objects=False, with_collections=False))

@app.route('/api/v1.0/getUsers', methods=['POST'])
@db_session
def getUsers():
    if request.method == 'POST':
        json_data = json.loads(request.data)
        with db_session:
            users = User.select()
            usuarios = []
            for user in users:
               usuarios.append(user.to_dict(
                   related_objects=False, with_collections=False))
            show(usuarios)
            return jsonify(usuarios)

@app.route('/api/v1.0/getAdventurePlayer', methods=['POST'])
@db_session
def getAdventurePlayer():
    if request.method == 'POST':
        json_data = json.loads(request.data)
        id_user = json_data['id_user']
        with db_session:
            user = User[id_user]
            result = user.to_dict(related_objects=True, with_collections=True)
            adventures = {}
            adventures['adventure'] = []
            for adv in result['adventures_players']:
                 adventures['adventure'].append(adv.to_dict(
                    related_objects=False, with_collections=False))
        return jsonify(adventures['adventure'])

@app.route('/api/v1.0/createAdventureMaster', methods=['POST'])
@db_session
def createAdventureMaster():
    if request.method == 'POST':
        json_data = json.loads(request.data)
        scr_img  = json_data['scr_img']
        title    = json_data['title']
        password_board = json_data['password']   
        id_user  = json_data['id_user']
        with db_session:
            user = User[id_user]
            new_adventure = Adventure(name_adventure=title, imagen_adventure=scr_img, password_adventure=password_board)
            new_board = Board(adventure=new_adventure,orden = 0)
            user.adventures_masters.add(new_adventure)
        return jsonify('Nuevo mensaje desde un servidor Flask' + title + scr_img)

@app.route('/api/v1.0/getAdventureMaster', methods=['POST'])
@db_session
def getAdventureMaster():
    if request.method == 'POST':
        json_data = json.loads(request.data)
        id_user  = json_data['id_user']
        print(id_user)
        with db_session:
            user = User[id_user]
            return to_dict(user)

@app.route('/api/v1.0/updateAdventureMaster', methods=['POST'])
def updateAdventure():
    if request.method == 'POST':
        json_data       = json.loads(request.data)
        scr_img         = json_data['scr_img']
        title           = json_data['title']
        password        = json_data['password']
        id_adventure    = json_data['id_adventure']
        with db_session:
            adventure = Adventure[id_adventure]
            adventure.name_adventure = title
            adventure.imagen_adventure = scr_img
            adventure.password_adventure = password
            return json.dumps('200')

@app.route('/api/v1.0/deleteAdventureMaster', methods=['POST'])
def deleteAdventure():
    if request.method == 'POST':
        json_data       = json.loads(request.data)
        id_adventure    = json_data['id_adventure']
        with db_session:
            Adventure[id_adventure].delete()
            return json.dumps('200')

@app.route('/api/v1.0/createFolder', methods=['POST'])
@db_session
def createFolder():
    if request.method == 'POST':
        json_data = json.loads(request.data)
        id_father_folder    = json_data['id_father_folder']
        name                = json_data['name']
        id_usuario          = json_data['id_usuario']
        with db_session:
            user = User[id_usuario]
            if id_father_folder != 0:
                folder = Folder[id_father_folder]
                new_folder = Folder(name_folder=name, user=user)
                folder.folders_son.add(new_folder)
            else:
                Folder(name_folder=name,user = user)
        return (to_dict(user.folders)) 

@app.route('/api/v1.0/getFolder', methods=['POST'])
@db_session
def getFolder(): 
    if request.method == 'POST':
        json_data = json.loads(request.data)
        id_user = json_data['id_user']
        id_father_folder = json_data['id_father_folder']
        
        with db_session:
            if id_father_folder != 0:
                folder = Folder[id_father_folder]
                father = {}
                father['folders_son'] = []
                father['file_folder'] = []
                if folder.folder_father:
                    father['folder_father'] = folder.folder_father.id_folder
                result = folder.to_dict(
                    related_objects=True, with_collections=True)
                for folders in result['folders_son']:
                    father['folders_son'].append(folders.to_dict(related_objects=False, with_collections=False))
                for files in result['file_folders']:
                    father['file_folder'].append(files.to_dict(related_objects=False, with_collections=False))
                return (father)
            else:
                folders = select((f.id_folder) for f in Folder if not f.folder_father)
                father = {}
                father['folders_son'] = []
                father['folder_father'] = 0
                for i in folders:
                    folder = Folder[i]
                    father['folders_son'].append(folder.to_dict(related_objects=False, with_collections=False))
                show(folders)
                return (father)

@app.route('/api/v1.0/updateNameFolder', methods=['POST'])
@db_session
def updateNameFolder():
    if request.method == 'POST':
        json_data = json.loads(request.data)
        id_folder = json_data['id_folder']
        name_folder = json_data['name_folder']
        with db_session:
            folder = Folder[id_folder]
            folder.name_folder = name_folder
            return '200'

@app.route('/api/v1.0/uploadImage', methods=['POST'])
@db_session
def uploadImage():
    upload_dir = '../public'
    fn=""
    file_names = []
    json_data = json.loads(request.values['data'])
    print(json_data['mykey'])
    # get file object from request.files (see: http://flask.pocoo.org/docs/1.0/api/#flask.Request.files)
    for key in request.files:
        file = request.files[key]
        print(type(file))
        fn = secure_filename(file.filename)
        file_names.append(fn)
        print(os.path.exists(upload_dir + '/' + fn))
        old_folder = Folder[json_data['mykey']]
        File_folder(name_file = 'test', url_file= fn, folder = old_folder)
        if not (os.path.exists(upload_dir + '/' + fn)):
            try:
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], fn))
            except:
                print(os.path.join(upload_dir, fn))
    return 'ok'

@app.route('/api/v1.0/getPlayersAdventure', methods=['POST'])
@db_session
def getPlayersAdventure():
    if request.method == 'POST':
        json_data = json.loads(request.data)
        players = []
        id_adventure = json_data['id_adventure']
        adventures = Adventure[id_adventure]
        show(adventures.to_dict(related_objects=True, with_collections=True))
        for player in adventures.players:
            players.append(player.to_dict(related_objects=False, with_collections=False))
            #usuarios.append(adventure.to_dict(related_objects=False, with_collections=False))
        return json.dumps(players)

@app.route('/api/v1.0/savePlayersAdventure', methods=['POST'])
@db_session
def savePlayersAdventure():
    if request.method == 'POST':
        json_data = json.loads(request.data)
        id_adventure = json_data['id_adventure']
        players = json_data['players']
        adv = Adventure[id_adventure]
        for player in players:
            user = User[player['id_user']]
            adv.players.add(user)
        return '200'

@app.route('/api/v1.0/insertTokenBoard', methods=['POST'])
@db_session
def insertTokenBoard():
    if request.method == 'POST':
        json_data = json.loads(request.data)
        id_board = json_data['id_board']
        src = json_data['src']
        x = json_data['x']
        y = json_data['y']
        width = json_data['width']
        height = json_data['height']
        board = Board[id_board]
        new_folder = Token(board=board, x=x, y=y,
                           width=width, height=height, src=src)
        return(new_folder.to_dict(related_objects=False, with_collections=False))

@app.route('/api/v1.0/getTokens', methods=['POST'])
@db_session
def getTokens(): 
    if request.method == 'POST':
        json_data = json.loads(request.data)
        id_board = json_data['id_board']
        with db_session:
            board = Board[id_board]
            result = board.to_dict(related_objects=True, with_collections=True)
            tokens = {}
            tokens['token'] = []
            for token in result['tokens']:
                tokens['token'].append(token.to_dict(related_objects=False, with_collections=False))
            return (tokens)

@app.route('/api/v1.0/updateTokens', methods=['POST'])
@db_session
def updateTokens(): 
    if request.method == 'POST':
        json_data = json.loads(request.data)
        id_token = json_data['id']
        x = json_data['x']
        y = json_data['y']
        width = json_data['width']
        height = json_data['height']
        with db_session:
            token = Token[id_token]
            token.x = x
            token.y = y
            token.width = width
            token.height = height
            return ('200')

@socketio.on('new_message')
def new_message(message):
    # Emitimos el mensaje con el alias y el mensaje del usuario
    emit('new_message', {
        'username': 'miguel',
        'text': 'test'
    }, broadcast=True)
    print('entro!!!!!!!')

@socketio.on('join')
def on_join(id_adventure):
    room = id_adventure
    join_room(room)
    print(request.sid)
    emit('new_message', {
    'username': 'miguel',
    'text': 'test'
    }, room=room)

@socketio.on('update_token')
def update_token(token):
    emit('update_token', token, room=token['id_adventure'])

@socketio.on('create_token')
def create_token(token):
    show(token)
    emit('create_token', token, room=token['id_adventure'])

if __name__ == '__main__':
    app.run(debug=True,host ='192.168.0.10')
